# rescript-service-worker

This package is the home of BuckleScript bindings for the
[JavaScript service worker 
API](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API). 
It is actively in use but has not been exhaustively tested.

## Long-term support

This package is currently supported but may go long periods of time without
fresh commits because it is something like "finished". Yes, software is like 
fashion, and fashion is never finished, and I will try to upgrade the ReScript 
language version from time to time, but unless there are significant defects 
in this project you can generally consider this project to be as stable as the 
ReScript language and the ServiceWorker API. 

This is to say: Don't be alarmed by the lack of recent commits! Stability is
good! And don't be afraid to open issues. 

## Installation
`npm i rescript-service-worker`

## Usage
This is meant to be a tight binding around the JavaScript API, and is intended
to feel idiomatic to JavaScript. Refer to 
[this article](https://webbureaucrat.gitlab.io/articles/writing-service-workers-in-rescript/) which documents usage from registration to caching to fetch 
event handling. 

## Implemented
- [X] [AbstractWorker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- [x] [Cache](https://developer.mozilla.org/en-US/docs/Web/API/Cache)
- [x] [CacheMatchOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/match)
- [x] [CacheDeleteOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/delete)
- [x] [CacheKeysOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/keys)
- [x] [CacheStorage](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorageJ)
- [x] [CacheStorageOptions](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorage/match)
- [x] [Client](https://developer.mozilla.org/en-US/docs/Web/API/Client)
- [x] [ClientsMatchAllOptions](https://developer.mozilla.org/en-US/docs/Web/API/Clients/matchAll)
- [x] [Clients](https://developer.mozilla.org/en-US/docs/Web/API/Clients)
- [X] [ContainerRegisterOptions](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register)
- [x] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent) 
- [x] [ExtendableMessageEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableMessageEvent)
- [x] [ExtendableMessageEventOptions](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableMessageEvent/ExtendableMessageEvent)
- [x] [ExtendableMessageEventSource](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableMessageEvent/ExtendableMessageEvent)
- [x] [FetchEvent](https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent)
- [X] [InstallEvent](https://developer.mozilla.org/en-US/docs/Web/API/InstallEvent)
- [X] [MessageEvent](https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent)
- [X] [MessagePort](https://developer.mozilla.org/en-US/docs/Web/API/MessagePort)
- [ ] NavigationPreloadManager
- [ ] NavigationPreload 
- [X] Navigator.serviceWorker (under a type called ServiceWorkerNavigator).
- [X] [PromiseRejectionEvent](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent)
- [X] [PromiseRejectionEventOptions](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent/PromiseRejectionEvent)
- [X] [PromiseRejectionEventType](https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent/PromiseRejectionEvent)
- [X] [ServiceWorker](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker)
- [X] [ServiceWorkerContainer](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer)
- [X] [ServiceWorkerGlobalScope](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope)
- [X] [ServiceWorkerNotificationOptions](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/getNotifications)
- [X] [ServiceWorkerRegistration](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration)
- [X] [ServiceWorkerState](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerState)
- [ ] SyncEvent
- [ ] SyncManager
- [X] [Worker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- [X] [WorkerCredentials](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [X] [WorkerOptions](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [X] [WorkerType](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [x] [WindowClient](https://developer.mozilla.org/en-US/docs/Web/API/WindowClient)

## Notes
### on EventTargets
I'm hoping to crack the code for how to perfectly enforce types onto 
`addEventListener`... someday. I haven't done it yet. For now, stick to the 
function-valued properties. Those are typed pretty well.

### on NavigationPreloadManager and NavigationPreloadState
My current thinking is to omit NavigationPreloadManager and 
NavigationPreloadState because they're too dependent on ByteString, and I 
don't want to expand the scope of my project. If you badly need these 
types or if 
you know of a ByteString implementation I could depend on, feel free to 
open an
issue.

### on ServiceWorkerGlobalScope
It's not fully implemented because it implements a ton of other interfaces.
Fully implementing it is a nice-to-have. If there's something you need from
this type that I haven't implemented, feel free to open an issue--it'll help
me prioritize. 

Also, currently the `navigator` property lives in 'ServiceWorkerGlobalScope',
even though technically it's a part of WorkerGlobalScope.

### on TODOs
I put in a TODO everyplace where there's a (non-deprecated) method or type
I didn't implement, whether or not I had any intention of implementing it. If
you go looking for something in here and find a TODO in its place, feel free
to open an issue or submit a pull request and we can talk about how to get
you what you need. 

## License

Because I have merely translated the API from the JavaScript specification
into ReScript with no implementation, I do not consider anything in this
repository to be copyrightable. 

As a courtesy, I have included a copy of the MIT license anyway just in case 
your lawyers are fussy about this sort of thing. If you would like a different
license, just let me know, and I will write one for you.
