type t =
  | Client(Client.t)
  | ServiceWorker(ServiceWorker.t)
  | MessagePort(MessagePort.t)
