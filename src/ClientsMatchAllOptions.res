type type_ = [
  | #window
  | #worker
  | #sharedWorker
  | #all
]

type clientsMatchAllOptions = {
  includeUncontrolled: bool,
  type_: type_,
}

type t = clientsMatchAllOptions
