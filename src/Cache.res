open Fetch

type cache

type req
external request: Request.t => req = "%identity"
external str: string => req = "%identity"

module Delete = {
  @send
  external withOptions: (
    cache,
    @unwrap [#Request(Request.t) | #String(string)],
    CacheDeleteOptions.t,
  ) => promise<bool> = "delete"

  @send
  external withoutOptions: (
    cache,
    @unwrap [#Request(Request.t) | #String(string)],
  ) => promise<bool> = "delete"
}

module Keys = {
  module WithRequest = {
    @send
    external withoutOptions: (
      cache,
      @unwrap [#Request(Request.t) | #String(string)],
    ) => promise<array<Request.t>> = "keys"

    @bs.send
    external withOptions: (
      cache,
      @unwrap [#Request(Request.t) | #String(string)],
      CacheMatchOptions.t,
    ) => promise<array<Request.t>> = "keys"
  }

  module WithoutRequest = {
    @send
    external withoutOptions: cache => promise<array<Request.t>> = "keys"

    @send external
    withOptions: (cache, CacheMatchOptions.t) => promise<array<Request.t>>
      = "keys"
  }
}

module Match = {
  @send external
  withoutOptions: (cache, Request.t) => promise<option<Response.t>>
    = "match"

  @send
  external withOptions: (
    cache,
    Request.t,
    CacheMatchOptions.t,
  ) => promise<option<Response.t>> = "match"
}

module MatchAll = {
  module WithRequest = {
    @send external
    withoutOptions: (cache, Request.t) => promise<array<Response.t>> = "match"

    @send
    external withOptions: (
      cache,
      Request.t,
      CacheMatchOptions.t,
    ) => promise<array<Response.t>> = "match"
  }

  module WithoutRequest = {
    @send
    external withoutOptions: cache => promise<array<Response.t>> = "match"

    @send external
    withOptions: (cache, CacheMatchOptions.t) => promise<array<Response.t>>
      = "match"
  }
}

@send external
add: (cache, @unwrap [#Request(Request.t) | #String(string)]) => promise<unit>
  = "add"

@send
external addAll: (cache, array<req>) => promise<unit> = "addAll"

@send
external put: (cache, Request.t, Response.t) => promise<unit> = "put"

type t = cache
