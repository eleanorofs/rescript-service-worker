type _t<'a>
type t_like<'a> = Dom.eventTarget_like<_t<'a>>
type t = t_like<Dom._baseClass>

module PostMessage = {
  @send external
  withTransferList: (t, 'message, 'transferList) => unit = "postMessage"

  @send
  external withoutTransferList: (t, 'message) => unit = "postMessage"
}

/* methods */
@send external start: t => unit = "start"

@send external close: t => unit = "close"

/* TODO Event Handlers
 * I'm not quite sure how to break the dependency cycle here. */
