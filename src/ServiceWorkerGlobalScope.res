/* TODO Okay, folks. This type inherits a ton of interfaces. I'd 
 * implement the
 * whole browser ecosystem before I'd finish this type if I really did it
 * right.

 * It looks like for most use cases, users of this type will mainly 
 * just need
 * the methods and properties that are specific to this type, so for the
 * moment, I'm going to operate on that assumption and just inherit
 * `Dom.eventTarget`. That said, if you need more, open an issue, and I'll
 * work on wht you need.
 */

type _t<'a>
type t_like<'a> = Dom.eventTarget_like<_t<'a>>
type t = t_like<Dom._baseClass>

@val external self: t = "self"

/* Properties */

@get external clients: t => array<Client.t> = "clients"

@get
external registration: t => ServiceWorkerRegistration.t = "registration"

@get external caches: t => CacheStorage.t = "caches"

/* inherited from WorkerGlobalScope */
@get external navigator: t => ServiceWorkerNavigator.t = "navigator"

/* Events */
type extendableEventHandler<'subtype>
  = Notifications.ExtendableEvent.t_like<'subtype> => unit

@get external
get_onactivate: t => option<extendableEventHandler<'subtype>> = "onactivate"

@set external
set_onactivate: (t, extendableEventHandler<'subtype>) => unit = "onactivate"

type handler = Dom.event => unit

@get
external get_oncontentdelete: t => option<handler> = "oncontentdelete"

@set
external set_oncontentdelete: (t, handler) => unit = "oncontentdelete"

type fetchHandler = FetchEvent.t => unit

@get
external get_onfetch: t => option<fetchHandler> = "onfetch"

@set external set_onfetch: (t, fetchHandler) => unit = "onfetch"

type installHandler = InstallEvent.t => unit

@get
external get_oninstall: t => option<installHandler> = "oninstall"

@set external set_oninstall: (t, installHandler) => unit = "oninstall"

type extendableMessageHandler = ExtendableMessageEvent.t => unit

@get
external get_onmessage: t => option<extendableMessageHandler> = "onmessage"

@set
external set_onmessage: (t, extendableMessageHandler) => unit = "onmessage"

type notificationHandler<'data>
  = Notifications.NotificationEvent.t<'data> => unit

@get external
get_onnotificationclick: t => option<notificationHandler<'data>>
  = "notificationclick"

@set external
set_onnotificationclick: (t, notificationHandler<'data>) => unit
  = "onnotificationclick"

@get external
get_onnotificationclose: t => option<notificationHandler<'data>>
  = "notificationclose"

@set external
set_onnotificationclose: (t, notificationHandler<'data>) => unit
  = "notificationclose"

type pushHandler<'data> = Push.PushEvent.t<'data> => unit

@get external get_onpush: t => option<pushHandler<'data>> = "onpush"

@set external set_onpush: (t, pushHandler<'data>) => unit = "onpush"

/* TODO onpushsubscriptionchange  requires PushSubscriptionChangeHandler,
 * which isn't in MDN
 * yet. */

/* TODO onsync requires SyncEvent */

/* Methods */
@send external skipWaiting: t => promise<unit> = "skipWaiting"

// TODO fetch should just use bs-fetch, right?
