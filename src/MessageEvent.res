type _t<'a>
type t_like<'a> = Dom.event_like<_t<'a>>
type t = t_like<Dom._baseClass>

/* properties */
@get external data: t => 'data = "data"

@get external origin: t => string = "origin"

@get external lastEventId: t => string = "lastEventId"

//TODO property source.

@get external ports: t => array<MessagePort.t> = "ports"
