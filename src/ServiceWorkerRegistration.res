type _t<'a>
type t_like<'a> = Dom.eventTarget_like<_t<'a>>
type t = t_like<Dom._baseClass>

module GetNotifications = {
  @send external
  withOptions: (t, ServiceWorkerNotificationOptions.t)
    => promise<array<Notifications.Notification.t<'data>>>
    = "getNotifications"

  @send external
  withoutOptions: t => promise<array<Notifications.Notification.t<'data>>>
    = "getNotifications"
}

module ShowNotification = {
  @send external
  withOptions: (t, string, Notifications.NotificationOptions.t<'data>)
    => promise<unit>
    = "showNotification"

  @send external
  withoutOptions: (t, string) => promise<unit> = "showNotification"
}

/* Properties */

@get external scope: t => string = "scope"

@get external installing: t => option<ServiceWorker.t> = "installing"

@get external waiting: t => option<ServiceWorker.t> = "waiting"

@get external active: t => option<ServiceWorker.t> = "active"

// TODO navigationPreload see README.

@get external pushManager: t => Push.PushManager.t = "pushManager"

/* TODO sync property - Requires the Background Synchronization API SyncManager
and isn't widely supported, so not prioritizing, but please open an issue if
you would like to see it.*/

// TODO periodicSync -- still experimental and hardly implemented 2024-11-23

/* Event Handlers */

type handler = unit => unit

@get external get_onupdatefound: t => promise<handler> = "onupdatefound"

@set external set_onupdatefound: (t, handler) => unit = "onupdatefound"

/* Methods */

@send external update: t => promise<t> = "update"

@send external unregister: t => promise<bool> = "unregister"
