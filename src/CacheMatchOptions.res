type cacheMatchOptions = {
  ignoreSearch: bool,
  ignoreMethod: bool,
  ignoreVary: bool,
}

type t = cacheMatchOptions
