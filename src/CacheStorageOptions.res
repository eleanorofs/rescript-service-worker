type cacheStorageOptions = {
  ignoreSearch: bool,
  ignoreMethod: bool,
  ignoreVary: bool,
  cacheName: string,
}

type t = cacheStorageOptions
