type _t<'a>
type t_like<'a> = Notifications.ExtendableEvent.t_like<_t<'a>>
type t = t_like<Dom._baseClass>

module Make = {
  @new external withoutInit: string => t = "ExtendableMessageEvent"
  @new external
  withInit: (string, ExtendableMessageEventOptions.t<'data>) => t
    = "ExtendableMessageEvent"
}

/* Properties */

@get external data: t_like<'subtype> => 'data = "data"

@get external origin: t_like<'subtype> => Client.t = "origin"

/* MDN says this is an empty string. I'm a little confused. */
@get
external lastEventId: t_like<'subtype> => string = "lastEventId"

/* MDN says this has to be a Client. I'm more than skeptical. In the
 * `init` type in the constructor it can be any `ExtendableMessageEventSource`.
 */
@get
external source: t_like<'subtype> => ExtendableMessageEventSource.t = "source"

@get
external ports: t_like<'subtype> => array<MessagePort.t> = "ports"
