type t<'data> = {
  data: 'data,
  origin: string,
  lastEventId: string,
  source: ExtendableMessageEventSource.t,
  ports: array<MessagePort.t>,
}
