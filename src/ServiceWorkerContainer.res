type t

/* Properties */

@get
external controller: t => option<ServiceWorker.t> = "controller"

@get external ready: t => option<unit> = "ready"

/* Events */

type handler = Dom.event => unit

@get external
get_oncontrollerchange: t => option<handler> = "oncontrollerchange"

@set
external set_oncontrollerchange: (t, handler) => unit = "oncontrollerchange"

type errorHandler = Dom.errorEvent => unit

@get
external get_onerror: t => option<errorHandler> = "onerror"

@set external set_onerror: (t, errorHandler) => unit = "onerror"

type messageHandler = MessageEvent.t => unit

@get
external get_onmessage: t => option<messageHandler> = "onmessage"

@set
external set_onmessage: (t, messageHandler) => unit = "onmessage"

/* Methods */
module Register = {
  @send
  external withOptions: (
    t,
    string,
    ContainerRegisterOptions.t,
  ) => promise<ServiceWorkerRegistration.t> = "register"

  @send external
  withoutOptions: (t, string) => promise<ServiceWorkerRegistration.t>
    = "register"
}

module GetRegistration = {
  @send external
  withoutScope: t => promise<option<ServiceWorkerRegistration.t>> = "register"

  @send external
  withScope: (t, string) => promise<option<ServiceWorkerRegistration.t>>
    = "register"
}

@send
external getRegistrations: t => promise<array<ServiceWorkerRegistration.t>>
  = "getRegistrations"

@send external startMessages: t => unit = "startMessages"
