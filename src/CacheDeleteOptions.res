type cacheDeleteOptions = {
  ignoreSearch: bool,
  ignoreMethod: bool,
  ignoreVary: bool,
  cacheName: string,
}

type t = cacheDeleteOptions
