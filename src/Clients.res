type clients

module MatchAll = {
  @send external
  withOptions: (clients, ClientsMatchAllOptions.t) => promise<array<Client.t>>
    = "matchAll"

  @send external
  withoutOptions: clients => Js.Promise.t<array<Client.t>> = "matchAll"
}

@send
external get: (clients, string) => promise<Client.t> = "get"

@send external
openWindow: (clients, string) => promise<option<WindowClient.t>> = "openWindow"

@send external claim: clients => promise<unit> = "claim"
