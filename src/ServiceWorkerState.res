type t = [
  | #installing
  | #installed
  | #activating
  | #activated
  | #redundant
]
