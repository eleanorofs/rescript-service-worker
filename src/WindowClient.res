type windowClient
type t = windowClient

module Impl = (
  T: {
    type t
  },
) => {
  type visibilityState = [
    | #hidden
    | #visibile
    | #prerender
  ]

  @get external focused: windowClient => bool = "focused"
  @get external
  visibilityState: windowClient => visibilityState = "visibilityState"

  @send external focus: windowClient => promise<windowClient> = "focus"

  @send external
  navigate: (windowClient, string) => promise<windowClient> = "navigate"
}

include Client.Impl({
  type t = t
})
