type _t<'subtype, 'a, 'reason>
type t_like<'subtype, 'a, 'reason> = Dom.event_like<_t<'subtype, 'a, 'reason>>
type t<'a, 'reason> = t_like<Dom._baseClass, 'a, 'reason>

/* Sometimes I wonder if my commitment to 80-character lines has gone too
 * far. */
@new
external make: (
  PromiseRejectionEventType.t,
  PromiseRejectionEventOptions.t<'a, 'reason>,
) => t<'a, 'reason> = "PromiseRejectionEvent"

/* Properties */
@get external promise: t<'a, 'reason> => promise<'a> = "promise"

@get external reason: t<'a, 'reason> => 'reason = "reason"

/* Event Handlers */

type handler<'a, 'reason> = t<'a, 'reason> => unit

@get external
get_onrejectionhandled: t<'a, 'reason> => handler<'a, 'reason>
  = "onrejectionhandled"

@set external
set_onrejectionhandled: (t<'a, 'reason>, handler<'a, 'reason>) => unit
  = "onrejectionhandled"

@get external
get_onunhandledrejection: t<'a, 'reason> => handler<'a, 'reason>
  = "onunhandledrejection"

@set
external set_onunhandledrejection: (t<'a, 'reason>, handler<'a, 'reason>)
  => unit
  = "onunhandledrejection"
