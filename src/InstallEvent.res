type _t<'a>
type t_like<'a> = Notifications.ExtendableEvent.t_like<_t<'a>>
type t = t_like<Dom._baseClass>

/* According to MDN, this type has a constructor and a property `activeWorker`,
 * both of which are obsolete. I'm not going to implement them, but I am
 * still going to leave this type here just in case more methods or properties
 * are added later. Open an issue if you need the obsolete functions.
 */
