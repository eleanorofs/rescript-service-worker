type t<'a, 'reason> = {
  promise: promise<'a>,
  reason: 'reason,
}
