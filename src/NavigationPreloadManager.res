type t

/* Methods */

@send external enable: t => Js.Promise.t<unit> = "enable"

@send external disable: t => Js.Promise.t<unit> = "disable"
