type _worker<'a>
type t_like<'a> = AbstractWorker.t_like<_worker<'a>>
type t = t_like<Dom._baseClass>

module Make = {
  @new external withoutOptions: string => t = "Worker"
  @new external withOptions: (string, WorkerOptions.t) => t = "Worker"
}

module PostMessage = {
  @send external
  withTransferList: (t_like<'subtype>, 'message, 'transferList) => unit
    = "postMessage"
  @send external
  withoutTransferList: (t_like<'subtype>, 'message) => unit = "postMessage"
}

/* Properties */

type messageHandler = MessageEvent.t => unit

@get external
get_onmessage: t_like<'subtype> => option<messageHandler> = "onmessage"

@set external
set_onmessage: (t_like<'subtype>, messageHandler) => unit = "onmessage"

type errorHandler = unit => unit

@get external
get_onmessageerror: t_like<'subtype> => option<errorHandler>
  = "onmessageerror"

@set external
set_onmessageerror: (t_like<'subtype>, errorHandler) => unit = "onmessageerror"

@get external
get_onrejectionhandled: PromiseRejectionEvent.t<'a, 'reason>
  => PromiseRejectionEvent.handler<'a, 'reason>
  = "onrejectionhandled"

@set external
set_onrejectionhandled: (
  PromiseRejectionEvent.t<'a, 'reason>,
  PromiseRejectionEvent.handler<'a, 'reason>,
) => unit = "onrejectionhandled"

@get external
get_onunhandledrejection: PromiseRejectionEvent.t<'a, 'reason>
  => PromiseRejectionEvent.handler<'a, 'reason>
  = "onunhandledrejection"

@set
external set_onunhandledrejection: (
  PromiseRejectionEvent.t<'a, 'reason>,
  PromiseRejectionEvent.handler<'a, 'reason>,
) => unit = "onunhandledrejection"

/* Methods */
@send external terminate: t_like<'subtype> => unit = "terminate"
