open Fetch

type cacheStorage

module Match = {
  @send
  external withOptions: (
    cacheStorage,
    Request.t,
    CacheStorageOptions.t,
  ) => promise<option<Response.t>> = "delete"

  @send external withoutOptions: (cacheStorage, Request.t)
    => promise<option<Response.t>>
    = "delete"
}

@send
external has: (cacheStorage, string) => promise<bool> = "has"

@send
external open_: (cacheStorage, string) => promise<Cache.t> = "open"

@send
external delete: (cacheStorage, string) => promise<bool> = "delete"

@send
external keys: cacheStorage => promise<array<string>> = "keys"

type t = cacheStorage
