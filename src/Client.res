type client

type t = client

module Impl = (
  T: {
    type t
  },
) => {
  @send
  external postMessage: (client, 'a) => unit = "postMessage"

  /* TODO postMessage but with a transfer object. I'm not sure how I want that
   to translate, y'know? #opentopullrequest */

  @get external id: client => string = "id"

  type type_ = [
    | #window
    | #worker
    | #sharedWorker
  ]

  @get external type_: client => type_ = "type"

  @get external url: client => string = "url"
}

include Impl({
  type t = t
})
