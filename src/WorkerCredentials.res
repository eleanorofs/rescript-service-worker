type t = [
  | #classic
  | #include_
  | @as("same-origin") #sameOrigin
  | #omit
]
