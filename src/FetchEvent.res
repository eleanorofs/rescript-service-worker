open Fetch

type _t<'a>
type t_like<'a> = Notifications.ExtendableEvent.t_like<_t<'a>>
type t = t_like<Dom._baseClass>

/* mdn calls this constructor "rarely used." */
@new external make: t = "FetchEvent"

/* properties */
@get external clientId: t => string = "clientId"
@get external preloadResponse: t => promise<Response.t> = "preloadResponse"
@get external replacesClientId: t => string = "replacesClientId"
@get external resultingClientId: t => string = "resultingClientId"
@get external request: t => Request.t = "request"

/* methods */
@send
external respondWith: (
  t,
  @unwrap [
      | #Resp(Response.t)
      | #Promise(promise<Response.t>)
  ]
) => unit = "respondWith"
