type _abstractWorker<'a>
type abstractWorker_like<'a> = Dom.eventTarget_like<_abstractWorker<'a>>
type abstractWorker = abstractWorker_like<Dom._baseClass>

type t = abstractWorker
type t_like<'a> = abstractWorker

type errorHandler = Dom.errorEvent => unit

@get
external getOnError: t_like<'subtype> => errorHandler = "onerror"
@set
external setOnError: (t_like<'subtype>, errorHandler) => unit = "onerror"
