/* Theoretically, ServiceWorker inherits from the Worker type, but not
 * in practice. In practice, according to MDN, Worker has one method that
 * ServiceWorker does not have (specifically, `terminate`), so I've copied over
 * all these properties and methods from Worker.
 */

type _t<'a>
type t_like<'a> = AbstractWorker.t_like<_t<'a>>
type t = t_like<Dom._baseClass>

module PostMessage = {
  @send external
  withTransferList: (t, 'message, 'transferList) => unit = "postMessage"

  @send
  external withoutTransferList: (t, 'message) => unit = "postMessage"
}

/* Properties */

type messageHandler = MessageEvent.t => unit

@get external get_onmessage: t => option<messageHandler> = "onmessage"

@set external set_onmessage: (t, messageHandler) => unit = "onmessage"

/* TODO is there a name for this type already? */
type errorHandler = unit => unit

@get external
get_onmessageerror: t => option<errorHandler> = "onmessageerror"

@set
external set_onmessageerror: (t, errorHandler) => unit = "onmessageerror"

@get external
get_onrejectionhandled: PromiseRejectionEvent.t<'a, 'reason>
  => PromiseRejectionEvent.handler<'a, 'reason>
  = "onrejectionhandled"

@set external
set_onrejectionhandled: (
  PromiseRejectionEvent.t<'a, 'reason>,
  PromiseRejectionEvent.handler<'a, 'reason>
) => unit = "onrejectionhandled"

@get external
get_onunhandledrejection: PromiseRejectionEvent.t<'a, 'reason>
  => PromiseRejectionEvent.handler<'a, 'reason>
  = "onunhandledrejection"

@set external
set_onunhandledrejection: (
  PromiseRejectionEvent.t<'a, 'reason>,
  PromiseRejectionEvent.handler<'a, 'reason>,
) => unit = "onunhandledrejection"

@get external scriptUrl: t => string = "scriptUrl"

@get external state: t => ServiceWorkerState.t = "state"
