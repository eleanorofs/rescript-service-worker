type cacheKeysOptions = {
  ignoreSearch: bool,
  ignoreMethod: bool,
  ignoreVary: bool,
  cacheName: string,
}

type t = cacheKeysOptions
